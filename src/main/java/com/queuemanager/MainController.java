package com.queuemanager;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class MainController {
    private boolean f = false;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody String printWelcome(ModelMap model) {
        model.addAttribute("message", "Hello world!");

        model.addAttribute("thing", getStrangeThing());
        return "hello";
    }

    public String getStrangeThing() {
        if (!f) {
            return "strangeThing";
        } else {
            return "notStrangeThing";
        }
    }
}