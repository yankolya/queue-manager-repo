define(function (require) {
	return function (app) {
		app.config(function ($routeProvider, $locationProvider, routers) {
			$locationProvider.html5Mode(true);
			$routeProvider.when(routers.home, {
				templateUrl: '/modules/home/views/main.html'
			}).when(routers.profile, {
				templateUrl: '/modules/profile/views/profile.html'
			}).when(routers.restorePassword, {
				templateUrl: '/modules/home/views/main.html'
			}).when(routers.pingPong, {
				templateUrl: '/modules/booking/views/pingPong.html'
			}).when(routers.billiard, {
				templateUrl: '/modules/booking/views/billiard.html'
			}).when(routers.gaming, {
				templateUrl: '/modules/booking/views/gaming.html'
			}).when(routers.categories, {
				templateUrl: '/modules/categories/views/main.html'
			});
			$routeProvider.otherwise({
				redirectTo: '/home'
			});
		});
	};
});
