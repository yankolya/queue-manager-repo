define(function (require) {
//	require modules
	var angular=require('angular');
	require('ngRoute');
	require('ngAnimate');
	require('ui_bootstrap_tpls');
	require('ngSanitize');
	require('dialogs');

//	my module
	require('../modules/header/index.js');
	require('../modules/home/index.js');
	require('../modules/widgets/index.js');
	require('../modules/footer/index.js');
    require('../modules/profile/index.js');
    require('../modules/booking/index.js');

// created module of app
	 var app=angular.module('app',
		 [
			 'ui.bootstrap',
			 'ngAnimate',
			 'ngRoute',
             'dialogs.main',
//			 my module
			 'header',
			 'home',
			 'footer',
             'widgets',
             'profile',
             'booking',
		 ]);

//	require  components of app
	require('./configs/constants')(app);
	require('./configs/value')(app);
	require('./configs/configs')(app);
	require('./configs/runs')(app);
	require('./configs/router')(app);
	require('./services/authInterceptor')(app);

	return app;
});
