'use strict';

var gulp = require('gulp');

// load plugins
var $ = require('gulp-load-plugins')();
var connect = require('connect');
var historyApiFallback = require('connect-history-api-fallback');

gulp.task('default', ['serve'], function () {
    gulp.start('serve');
});

gulp.task('connect', function () {

    var app = connect()
        .use(require('connect-livereload')({ port: 35729 }))
        .use(historyApiFallback)
		.use(connect.static('.'))
        .use(connect.directory('.'));


    require('http').createServer(app)
        .listen(9000)
        .on('listening', function () {
            console.log('Started connect web server on http://localhost:9000');
        });
});

gulp.task('serve', ['connect'], function () {
    require('opn')('http://localhost:9000');
});

gulp.task('watch', ['connect', 'serve'], function () {
    var server = $.livereload();

    // watch for changes
    gulp.watch([
       '.app/**/*.json',
       '.app/**/*.css',
       '.app/**/*.html',
       '.app/**/*.js',
       '.module/**/*.json',
       '.module/**/*.css',
       '.module/**/*.html',
       '.module/**/*.js',
       '.common/**/*.json',
       '.common/**/*.css',
       '.common/**/*.html',
       '.common/**/*.js'
    ]).on('change', function (file) {
        server.changed(file.path);
    });

});
