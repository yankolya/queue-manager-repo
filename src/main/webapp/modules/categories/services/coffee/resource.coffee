define (require)->
  (module)->
    module.factory 'categories.$resource',
      [
        '$resource',
          ($resource)->

            lang:$resource '/modules/categories/json/lang/:lang',
             { lang: 'en.json' }

            categoriesList:$resource '/modules/categories/json/categories.json'
      ]
