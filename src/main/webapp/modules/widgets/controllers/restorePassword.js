// Generated by CoffeeScript 1.8.0
define(function(require) {
  return function(module) {
    require('../services/resource.js')(module);
    return module.controller('widgets.ctrl.restorePassword', function($scope, widgets_$resource, $location, $rootScope, $modalInstance, $routeParams, $parseError) {
      $scope.server_errs = {};
      $scope.restorePassword = $scope.restorePassword || {};
      $scope.lang = widgets_$resource.lang.get();
      $scope.$parent.dismissModal = function() {
        return $modalInstance.close();
      };
      $scope.errorsHide = function(err_name, status) {
        if (status == null) {
          status = true;
        }
        if ($scope.server_errs.hasOwnProperty(err_name)) {
          return $scope.server_errs[err_name].show = !status;
        }
      };
      $scope.cancel = function() {
        return $modalInstance.dismiss('cancel');
      };
      return $scope.submit = function(form) {
        $scope.restorePassword.code = $routeParams.code;
        if (form.$valid) {
          $parseError.setErrObj($scope.restorePassword);
          return widgets_$resource.restorePassword($scope.restorePassword).then(function(res) {
            $rootScope.$broadcast('restorePassword', res);
            $modalInstance.close();
            return $location.path('/');
          }, function(err) {
            return $scope.server_errs = $parseError.getErrs(err.data);
          });
        } else {
          return angular.forEach(form.$error.required, function(el) {
            return el.$pristine = false;
          });
        }
      };
    });
  };
});
