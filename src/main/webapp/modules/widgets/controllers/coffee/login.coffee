'use strict'
define (require)->
  (module)->
    require('../services/resource.js')(module);

    module.controller 'widgets.ctrl.login',
    ($scope, widgets_$resource, $location, $rootScope, $modalInstance,$parseError)->
#					values
#					  var

#					  $scope
      $scope.server_errs={}
      $scope.login = {}
      $scope.lang = widgets_$resource.lang.get()

      #					  $rootScope

      #					events

      #					buttons
      $scope.$parent.dismissModal = ->
        $modalInstance.close()

      $scope.errorsHide = (err_name,status=true)->
        if $scope.server_errs.hasOwnProperty err_name
          $scope.server_errs[err_name].show=!status

      $scope.cancel = ->
        $modalInstance.dismiss('cancel')

      $scope.submit = (form)->
        if form.$valid
          $parseError.setErrObj $scope.login
          widgets_$resource.login($scope.login)
          .then(
            (res)->
              $rootScope.$broadcast 'login', res, $scope.login.remember
              $modalInstance.close()
            (err)->
              $scope.server_errs=$parseError.getErrs err.data
          )
        else
          angular.forEach form.$error.required,(el)->
            el.$pristine = false
#					watches
