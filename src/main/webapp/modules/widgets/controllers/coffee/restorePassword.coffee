define (require)->
  (module)->
    require('../services/resource.js')(module);

    module.controller 'widgets.ctrl.restorePassword',
    ($scope, widgets_$resource, $location, $rootScope, $modalInstance, $routeParams, $parseError)->
      #					values
      #					var
      #					$scope
      $scope.server_errs={}
      $scope.restorePassword = $scope.restorePassword || {}
      $scope.lang = widgets_$resource.lang.get()

      #					$rootScope
      #					events
      #					buttons
      $scope.$parent.dismissModal = ->
        $modalInstance.close()

      $scope.errorsHide = (err_name,status=true)->
        if $scope.server_errs.hasOwnProperty err_name
          $scope.server_errs[err_name].show=!status


      $scope.cancel = ->
        $modalInstance.dismiss('cancel')

      $scope.submit = (form)->
        $scope.restorePassword.code = $routeParams.code
        if form.$valid
          $parseError.setErrObj $scope.restorePassword

          widgets_$resource.restorePassword($scope.restorePassword)
          .then(
            (res)->
              $rootScope.$broadcast 'restorePassword', res
              $modalInstance.close()
              $location.path('/')
            (err)->
              $scope.server_errs=$parseError.getErrs err.data

          )
        else
          angular.forEach form.$error.required,(el)->
            el.$pristine = false
#					watches

