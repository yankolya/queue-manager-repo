define (require)->
  (module)->
    require('../services/resource.js')(module);

    module.controller 'widgets.ctrl.infoModals',
    ($scope, widgets_$resource, $location, $rootScope, $modalInstance, $timeout)->
      #					values
      #					  var
      #					  $scope
      $scope.lang = widgets_$resource.lang.get()
      #					  $rootScope
      #					events
      #					buttons
      $scope.$parent.dismissModal = ->
        $modalInstance.close()

      $scope.cancel = ->
        $modalInstance.dismiss('cancel')
#					watches

