define (require)->
  (module)->
    require('../services/resource.js')(module);

    module.controller 'widgets.ctrl.register',
    ($scope, widgets_$resource, $location, $rootScope, $modalInstance, $timeout, $parseError)->
#				values
#				  var
#				  $scope
      $scope.registerObj = {}
      $scope.server_errs = {}
      $scope.lang = widgets_$resource.lang.get()
      #				  $rootScope'
      #				events
      #				buttons
      $scope.$parent.dismissModal = ->
        $modalInstance.close()

      $scope.errorsHide = (err_name, status = true)->
        if $scope.server_errs.hasOwnProperty err_name
          $scope.server_errs[err_name].show = !status

      $scope.cancel = ->
        $modalInstance.dismiss('cancel')

      $scope.submit = (form) ->
        if form.$valid && form.password_confirm.$modelValue == form.password.$modelValue
          data = angular.copy($scope.registerObj)
          data.country = $scope.registerObj.country.code
          $parseError.setErrObj(data)

          widgets_$resource.createUser(data)
          .then(
            (res)->
              $rootScope.$broadcast 'login', res, false
              $modalInstance.close()
            (err)->
              $scope.server_errs = $parseError.getErrs(err.data)
          )
        else
          angular.forEach form.$error.required,(el)->
            el.$pristine = false
#				watches

