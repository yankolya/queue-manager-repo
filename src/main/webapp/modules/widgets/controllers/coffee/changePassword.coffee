define (require)->
  (module)->
    require('../services/resource.js')(module);

    module.controller 'widgets.ctrl.changePassword',
    ($scope, widgets_$resource, $location, $rootScope, $modalInstance, $parseError)->
      #					values
      #					  var
      #					  $scope
      $scope.changePassword = $scope.changePassword || {}
      $scope.lang = widgets_$resource.lang.get()
      $scope.server_errs={}
      #					  $rootScope
      #					buttons
      $scope.$parent.dismissModal = ->
        $modalInstance.close()

      $scope.errorsHide = (err_name,status=true)->
        if $scope.server_errs.hasOwnProperty err_name
          $scope.server_errs[err_name].show=!status


      $scope.cancel = ->
        $modalInstance.dismiss('cancel')

      $scope.submit = (form)->
        if form.$valid
          $parseError.setErrObj $scope.changePassword

          widgets_$resource.changePassword($scope.changePassword)
          .then(
            (res)->
              $rootScope.$broadcast 'changePassword', res
              $modalInstance.close()

            (err)->
              $scope.server_errs=$parseError.getErrs err.data
          )
        else
          angular.forEach form.$error.required,(el)->
            el.$pristine = false
#					watches
#					events

