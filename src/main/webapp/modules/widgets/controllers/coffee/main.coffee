define (require)->
  (module)->
    require('../services/resource.js')(module);

    module.controller 'widgets',
    ($scope, widgets_$resource, $rootScope, $routeParams, $modal, $location, isModalOpen)->
#					values
#					  var
#					  $scope
      $scope.lang = widgets_$resource.lang.get()
      #					  $rootScope
      #					events
      $rootScope.$on '$routeChangeStart', (e, route)->
        if route.params.modal
          switch route.params.modal
            when 'login' then $scope.login()
            when 'register' then $scope.register()
            when 'change-password' then $scope.changePasswordOpen()
            when 'forgot-password' then $scope.forgotPasswordOpen()
            when 'video-guidelines' then $scope.videoGuidelines()
            when 'challenge-guidelines' then $scope.challengeGuidelines()
            when 'judging-info' then $scope.judgingInfo()

        else if route.params.code
          $scope.restorePasswordOpen()

        else if isModalOpen
          $scope.dismissModal()

      #					buttons
      $scope.dismissModal = ->
        return null

      $scope.openModal = (templateUrl, controller, size)->
        isModalOpen = true

        modalInstance = $modal.open
          templateUrl: templateUrl
          controller: controller
          scope: $scope
          size: size
          windowClass: 'widgets'
        modalInstance.result.then $scope.closeModal, $scope.closeModal


      $scope.closeModal = ->
        isModalOpen = false
        path = $location.path().split '/'
        path.pop()
        path = path.join '/'
        $location.skipReload().path path

      $scope.login = (size)->
        $scope.openModal '/modules/widgets/views/login.html', 'widgets.ctrl.login', size

      $scope.register = (size)->
        $scope.openModal '/modules/widgets/views/register.html', 'widgets.ctrl.register', size

      $scope.changePasswordOpen = (size)->
        $scope.openModal '/modules/widgets/views/changePassword.html', 'widgets.ctrl.changePassword', size

      $scope.forgotPasswordOpen = (size)->
        $scope.openModal '/modules/widgets/views/forgotPassword.html', 'widgets.ctrl.forgotPassword', size

      $scope.videoGuidelines = (size)->
        $scope.openModal '/modules/widgets/views/videoGuidelines.html', 'widgets.ctrl.infoModals', size

      $scope.challengeGuidelines = (size)->
        $scope.openModal '/modules/widgets/views/challengeGuidelines.html', 'widgets.ctrl.infoModals', size

      $scope.judgingInfo = (size)->
        $scope.openModal '/modules/widgets/views/judgingInfo.html', 'widgets.ctrl.infoModals', size

      $scope.restorePasswordOpen = (size)->
        $scope.openModal '/modules/widgets/views/restorePassword.html', 'widgets.ctrl.restorePassword', size


