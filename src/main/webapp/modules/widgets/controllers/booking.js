define(function (require) {
	return function (module) {
		require('../services/resource.js')(module);
		module.controller('widgets.ctrl.changePassword', function ($scope, widgets_$resource, $location, $rootScope, $modalInstance, $parseError) {
			$scope.changePassword = $scope.changePassword || {};
			$scope.lang = widgets_$resource.lang.get();
			$scope.server_errs = {};
			$scope.$parent.dismissModal = function () {
				$modalInstance.close();
			};
			$scope.errorsHide = function (err_name, status) {
				if (status == null) {
					status = true;
				}
				if ($scope.server_errs.hasOwnProperty(err_name)) {
					$scope.server_errs[err_name].show = !status;
				}
			};
			$scope.cancel = function () {
				$modalInstance.dismiss('cancel');
			};
			$scope.submit = function (form) {
				if (form.$valid) {
					$parseError.setErrObj($scope.changePassword);
					widgets_$resource.changePassword($scope.changePassword).then(function (res) {
						$rootScope.$broadcast('changePassword', res);
						$modalInstance.close();
					}, function (err) {
						$scope.server_errs = $parseError.getErrs(err.data);
					});
				} else {
					angular.forEach(form.$error.required, function (el) {
						el.$pristine = false;
					});
				}
			};
		});
	};
});
