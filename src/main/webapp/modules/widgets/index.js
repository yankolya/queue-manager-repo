define(function (require) {

	require('ngResource');

   var module = angular.module('widgets', ['ngResource']);

//   common files
   require('../../common/services/parseErrorRes.js')(module);

//  module files
	require('./configs/values.js')(module);
	require('./controllers/main.js')(module);
	require('./controllers/login.js')(module);
	require('./controllers/register.js')(module);
	require('./controllers/changePassword.js')(module);
   require('./controllers/forgotPassword.js')(module);
   require('./controllers/infoModals.js')(module);
   require('./controllers/restorePassword.js')(module);


	return module;
});