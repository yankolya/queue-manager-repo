define (require)->
  (module)->
    module.factory 'widgets_$resource',
      ($resource,apiUrl,langUrl)->

        createRequest=(url,data)->
          req=$resource apiUrl+url
          newReq= new req()

          angular.extend(newReq,data)
          newReq.$save()

        lang:$resource langUrl+'modules/widgets/json/lang/:lang',
          lang: 'en.json' ,
              get:
                cache:true

        createUser:(data)->
          createRequest('user/register',data)

        login:(data)->
          createRequest('user/login',data)

        changePassword:(data)->
          createRequest('user/change',data)

        forgotPassword:(data)->
          createRequest('user/forgot',data)

        restorePassword:(data)->
          createRequest('user/restore',data)
