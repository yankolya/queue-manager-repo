define (require)->
  (module)->
    module.factory '$parseError', ()->
      errors_obj={}

      property_body=
        show:false
        msg:null

      setErrObj= (o)->
        props=angular.copy(o)

        for key of props
          props[key]=angular.copy property_body

        errors_obj=props

      getErrObj= ->
        errors_obj

      getErrs=(data)->
        data.forEach (item)->
          message= if typeof item.message is 'string' then item.message else
            item.message.join('/n')
          if errors_obj.hasOwnProperty item.field
            errors_obj[item.field].msg=message
            errors_obj[item.field].show=true
          else
            throw 'property of error: '+item.field+' not found'
        return errors_obj


      setErrObj:setErrObj
      getErrObj:getErrObj
      getErrs:getErrs


