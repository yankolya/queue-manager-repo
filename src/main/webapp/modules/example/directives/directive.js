// Generated by CoffeeScript 1.8.0
define(function(require) {
  return function(module) {
    require('../../services/service.js')(module);
    require('../../services/resource.js')(module);
    require('../../services/provider.js')(module);
    return module.directive('directiveName', function(injectables) {
      return {
        priority: 0,
        require: '^name',
        template: '<div></div>',
        templateUrl: 'directive.html',
        replace: false,
        transclude: false,
        restrict: 'EACM',
        scope: false,
        compile: function(tElement, tAttrs, transclude) {
          return {
            pre: function(scope, iElement, iAttrs, controller) {},
            post: function(scope, iElement, iAttrs, controller) {}
          };
        },
        link: function(scope, iElement, iAttrs) {}
      };
    });
  };
});
