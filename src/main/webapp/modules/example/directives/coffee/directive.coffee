define (require)->
  (module)->
    require('../../services/service.js')(module);
    require('../../services/resource.js')(module);
    require('../../services/provider.js')(module);

    module.directive 'directiveName', (injectables)->
      priority: 0
      require: '^name'
      template: '<div></div>'
      templateUrl: 'directive.html'
      replace: false
      transclude: false
      restrict: 'EACM'
      scope: false
      compile: (tElement, tAttrs, transclude)->
        pre: (scope, iElement, iAttrs, controller)->
        post: (scope, iElement, iAttrs, controller)->
      link: (scope, iElement, iAttrs)->
