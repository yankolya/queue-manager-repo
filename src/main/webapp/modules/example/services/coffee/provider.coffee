define (require)->
  (module)->
    module.provider 'provider', ()->
      #   Private variables
      salutation = 'Hello'

      #   Private constructor
      Greeter = ()->
        @greet = ()->
          salutation
        return

      #   Public API for configuration
      @setSalutation = (s)->
        salutation = s


      #   Method for instantiating
      @$get = ()->
        new Greeter()

      return
