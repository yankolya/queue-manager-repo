define (require)->
  (module)->
    module.factory '$factory', ()->
#         Service logic
#         ...
      meaningOfLife = 42

      #         Public API here

      someMethod: ()->
        meaningOfLife

