define (require)->
  (module)->
    module.factory 'example_$resource', ($resource)->
      $resource 'url/:id',
          id: 'example' ,
            update:
              method: 'PUT'
            lang:
              method: 'GET'
              cache: true
              url: '/modules/example/json/lang.json'

