define (require)->
  (module)->
    require('../../services/service.js')(module);
    require('../../services/resource.js')(module);
    require('../../services/provider.js')(module);

    module.filter 'filter', ()->
      (items, name)->
        arrayToReturn = [];
        items.forEach (item)->
          if item.name != name
            arrayToReturn.push(item)

        return arrayToReturn
