// Generated by CoffeeScript 1.8.0
define(function(require) {
  return function(module) {
    require('../../services/service.js')(module);
    require('../../services/resource.js')(module);
    require('../../services/provider.js')(module);
    return module.filter('filter', function() {
      return function(items, name) {
        var arrayToReturn;
        arrayToReturn = [];
        items.forEach(function(item) {
          if (item.name !== name) {
            return arrayToReturn.push(item);
          }
        });
        return arrayToReturn;
      };
    });
  };
});
