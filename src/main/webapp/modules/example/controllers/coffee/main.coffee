define (require)->
  (module)->
    require('../services/service.js')(module);
    require('../services/resource.js')(module);
    require('../services/provider.js')(module);

    module.controller 'example',
    ($scope, example_$resource, service, provider)->
#					values
#					  var
      hello = provider.greet()
      who = service.run()

      #					  $scope
      console.log example_$resource
      $scope.example = hello + '. ' + who
      $scope.lang = example_$resource.lang()

#					  $rootScope

#					buttons

#					watches

#					events

