// Generated by CoffeeScript 1.8.0
define(function(require) {
  return function(module) {
    require('../services/service.js')(module);
    require('../services/resource.js')(module);
    require('../services/provider.js')(module);
    return module.controller('example', function($scope, example_$resource, service, provider) {
      var hello, who;
      hello = provider.greet();
      who = service.run();
      console.log(example_$resource);
      $scope.example = hello + '. ' + who;
      return $scope.lang = example_$resource.lang();
    });
  };
});
