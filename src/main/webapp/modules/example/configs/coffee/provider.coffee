define (require)->
 (module)->
   require('../services/provider.js')(module)

   module.config (providerProvider)->
    providerProvider.setSalutation 'Hello World'