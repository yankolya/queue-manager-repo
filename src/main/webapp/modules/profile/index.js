define(function (require) {

    var angular=require('angular');
	require('ngResource');
    require('ngImgCrop');

	var module = angular.module('profile', ['ngResource', 'ngImgCrop']);

	require('./controllers/main.js')(module);
    require('./filters/range.js')(module);
    require('./directives/getImgBase64.js')(module);


	return module;
});