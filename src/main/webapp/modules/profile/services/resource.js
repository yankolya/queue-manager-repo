define(function (require) {
	return function (module) {
		return module.factory('profile_$resource', function ($resource, apiUrl, langUrl) {
			return {
				lang: $resource(langUrl + 'modules/profile/json/lang/:lang', {
					lang: 'en.json'
				}, {
					get: {
						cache: true
					}
				}),
				profileData: $resource(langUrl + 'modules/profile/json/profile.json', {}, {
					get: {
						cache: true
					}
				}),
				profile: function (data) {
					var res, user;
					res = $resource(apiUrl + 'profile', {}, {
						save: {
							method: 'POST',
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'x-requested-with': 'XMLHttpRequest'
							}
						}
					});
					user = new res;
					angular.extend(user, data);
					return user.$save();
				}
			};
		});
	};
});
