define (require)->
  (module)->
    module.factory 'profile_$resource',
    ($resource,apiUrl,langUrl)->
      lang: $resource langUrl+'modules/profile/json/lang/:lang',
        lang: 'en.json' ,
          get:
            cache:true


      profileData: $resource langUrl+'modules/profile/json/profile.json',
        {}
          get:
            cache:true

      profile: (data)->
        res = $resource apiUrl+'profile',
          {}
            save:
              method:
                'POST'
              headers:
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-requested-with': 'XMLHttpRequest'


        user = new res
        angular.extend(user, data)
        user.$save()
