define (require)->
  (module)->

    module.filter 'range',
      [
        ()->
          (val, range)->
            for i in [1..range] by 1
              val.push(i);
            return val;
      ]

