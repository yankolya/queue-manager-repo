define (require)->
  (module)->

    module.directive 'getImgBase', ->
      restrict: 'A'
      link: (scope, iElement, iAttrs)->

        handleFileSelect=(evt) ->
          file=evt.currentTarget.files[0]
          reader = new FileReader()
          reader.onload = (evt) ->
            scope.$apply ($scope) ->
              scope[iAttrs.getImgBase]=evt.target.result

          reader.readAsDataURL file

        iElement.on 'change',handleFileSelect
