// Generated by CoffeeScript 1.8.0
define(function(require) {
  return function(module) {
    return module.directive('getImgBase', function() {
      return {
        restrict: 'A',
        link: function(scope, iElement, iAttrs) {
          var handleFileSelect;
          handleFileSelect = function(evt) {
            var file, reader;
            file = evt.currentTarget.files[0];
            reader = new FileReader();
            reader.onload = function(evt) {
              return scope.$apply(function($scope) {
                return scope[iAttrs.getImgBase] = evt.target.result;
              });
            };
            return reader.readAsDataURL(file);
          };
          return iElement.on('change', handleFileSelect);
        }
      };
    });
  };
});
