define(function (require) {
	return function (module) {
		require('../services/resource.js')(module);
		return module.controller('profile.ctrl.main', function ($scope, $rootScope, user, profile_$resource, $routeParams, $http, $parseError, $timeout) {
			$scope.init = function () {
				$scope.user = {
					profile: {}
				};
				$scope.showEditBlock = $routeParams.userId === user.id;
				if ($scope.showEditBlock) {
					if (Object.keys(user.profile).length === 0) {
						profile_$resource.profileData.get({
							userId: $routeParams.userId
						}, function (data) {
							user.profile = data;
							$scope.profile = {
								email: user.email,
								about: data.about,
								age: data.age,
								judge: data.judge
							};
							$scope.user = user;
						});
					} else {
						$scope.user = user;
						$scope.profile = user.profile;
						$scope.profile.email = user.email;
					}
				} else {
					profile_$resource.profileData.get({
						userId: $routeParams.userId
					}, function (data) {
						$scope.user.profile = data;
					});
				}
			};
			$scope.errorsHide = function (err_name, status) {
				if (status == null) {
					status = true;
				}
				if ($scope.server_errs.hasOwnProperty(err_name)) {
					$scope.server_errs[err_name].show = !status;
				}
			};
			$scope.server_errs = {};
			$scope.myImage = '';
			$scope.myCroppedImage = '';
			$scope.lang = profile_$resource.lang.get();
			$scope.profile = $scope.profile || {};
			$scope.submit = function (form) {
				if (form.$valid) {
					$parseError.setErrObj($scope.profile);
					$scope.profile.avatar = $scope.myCroppedImage;
					profile_$resource.profile($scope.profile).then(function (res) {
						$rootScope.$broadcast('profile', res);
					}, function (err) {
						$scope.server_errs = $parseError.getErrs(err.data);
					});
				} else {
					angular.forEach(form.$error.required, function (el) {
						el.$pristine = false;
					});
				}
			};
		});
	};
});
