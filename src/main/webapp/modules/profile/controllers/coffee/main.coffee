define (require)->
  (module)->
    require('../services/resource.js')(module);

    module.controller 'profile.ctrl.main',
    ($scope, $rootScope, user, profile_$resource, $routeParams, $http, $parseError, $timeout)->
#					values
#					  var
#					  $scope

      $scope.init = ->
        $scope.user = {
          profile: {
          }
        }
        $scope.showEditBlock = $routeParams.userId == user.id
        if $scope.showEditBlock
          if Object.keys(user.profile).length == 0
            profile_$resource.profileData.get {userId: $routeParams.userId}, (data) ->
              user.profile = data
              $scope.profile = {
                email: user.email,
                about: data.about
                age: data.age
                judge: data.judge
              }
              $scope.user = user
          else
            $scope.user = user
            $scope.profile = user.profile
            $scope.profile.email = user.email
        else
          profile_$resource.profileData.get {userId: $routeParams.userId}, (data) ->
            $scope.user.profile = data


      $scope.errorsHide = (err_name,status=true)->
        if $scope.server_errs.hasOwnProperty err_name
          $scope.server_errs[err_name].show=!status

      $scope.server_errs={}
      $scope.myImage = ''
      $scope.myCroppedImage = ''
      $scope.lang = profile_$resource.lang.get()

      $scope.profile = $scope.profile || {}
      #					  $rootScope
      #					buttons
      $scope.submit = (form)->
        if form.$valid
          $parseError.setErrObj $scope.profile
          $scope.profile.avatar = $scope.myCroppedImage
          profile_$resource.profile($scope.profile)
          .then(
            (res)->
              $rootScope.$broadcast 'profile', res
            (err)->
              $scope.server_errs = $parseError.getErrs err.data
          )
        else
          angular.forEach form.$error.required, (el)->
            el.$pristine = false

#					watches


