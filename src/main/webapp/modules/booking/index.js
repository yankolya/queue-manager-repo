define(function (require) {

    var angular=require('angular');
	require('ngResource');

	var module = angular.module('booking', ['ngResource']);

	require('./controllers/main.js')(module);
	require('./controllers/pingPong.js')(module);
	require('./controllers/billiard.js')(module);
	require('./controllers/gaming.js')(module);


	return module;
});