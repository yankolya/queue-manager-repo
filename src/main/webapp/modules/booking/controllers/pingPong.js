define(function (require) {
	return function (module) {
		require('../services/resource.js')(module);
		module.controller('booking.ctrl.pingPong', function ($scope, $rootScope, booking_$resource, user, $routeParams) {
			$scope.lang = booking_$resource.lang.get();
			$scope.pingPongList = booking_$resource.getPingPongList.get();


			$scope.user = user;
			$scope.activeTab = 'pingPong';
		});
	};
});
