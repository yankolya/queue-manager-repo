define(function (require) {
	return function (module) {
		require('../services/resource.js')(module);
		module.controller('booking.ctrl.gaming', function ($scope, $rootScope, booking_$resource, user) {
			$scope.lang = booking_$resource.lang.get();
			$scope.gamingList = booking_$resource.getGamingList.get();

			$scope.user = user;
			$scope.activeTab = 'gaming';
		});
	};
});
