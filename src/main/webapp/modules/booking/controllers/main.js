define(function (require) {
	return function (module) {
		require('../services/resource.js')(module);
		module.controller('booking.ctrl.main', function ($scope, $rootScope, booking_$resource) {
			$scope.lang = booking_$resource.lang.get();
		});
	};
});
