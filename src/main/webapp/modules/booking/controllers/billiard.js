define(function (require) {
	return function (module) {
		require('../services/resource.js')(module);
		module.controller('booking.ctrl.billiard', function ($scope, $rootScope, booking_$resource, user) {
			$scope.lang = booking_$resource.lang.get();
			$scope.billiardList = booking_$resource.getBilliardList.get();

			$scope.user = user;
			$scope.activeTab = 'billiard';
		});
	};
});
