define(function (require) {
	return function (module) {
		return module.factory('booking_$resource', [
			'$resource', function ($resource, langUrl) {
				return {
					lang: $resource('/modules/booking/json/lang/:lang', {
						lang: 'en.json'
					}, {
						update: {
							method: 'PUT'
						},
						get: {
							cache: true
						}
					}),
					getPingPongList: $resource('/modules/booking/json/pingPongList.json', {}, {
						get: {
							cache: true
						}
					}),
					getBilliardList: $resource('/modules/booking/json/billiardList.json', {}, {
						get: {
							cache: true
						}
					}),
					getGamingList: $resource('/modules/booking/json/gamingList.json', {}, {
						get: {
							cache: true
						}
					}),

				};
			}
		]);
	};
});
