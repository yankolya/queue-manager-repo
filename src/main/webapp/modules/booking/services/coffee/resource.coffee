define (require)->
  (module)->
    module.factory 'dashboard_$resource',
      [
        '$resource',
        ($resource)->

          lang:$resource '/modules/dashboard/json/lang/:lang',
            { lang: 'en.json' }
              {
                update:
                  method: 'PUT'
                get:
                  cache: true
              }

      ]
