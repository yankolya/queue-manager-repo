define (require)->
  (module)->
    module.factory 'home.$resource',
      [
        '$resource',
          ($resource)->

            lang:$resource '/modules/home/json/lang/:lang',
             { lang: 'en.json' }
              {
               update: { method: 'PUT' }
               cache:true
             }
            videos:$resource '/modules/home/json/videos.json',
              {  }
                {
                  update: { method: 'PUT' }
                  cache:true
                }
            challenges:$resource '/modules/home/json/challenges.json',
              {  }
                {
                  update: { method: 'PUT' }
                  cache:true
                }
      ]
